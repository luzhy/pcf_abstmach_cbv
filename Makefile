
SRC= syntax.ml iparse.mly lex.mll util.ml eval.ml main.ml main_exe.ml
COMPONENT= syntax.ml iparse.mli iparse.ml lex.ml util.ml eval.ml main.ml 
TARGET= PCF_AbstMach_CbV

all:	$(TARGET)

$(TARGET): 	$(COMPONENT)
	ocamlc     -rectypes $(COMPONENT) -o $(TARGET) 

iparse.mli:	iparse.mly
	ocamlyacc -v iparse.mly

iparse.ml:	iparse.mly
	ocamlyacc -v iparse.mly

lex.ml:	lex.mll
	ocamllex lex.mll

backup:
	/bin/cp -f Makefile $(SRC) back

clean:
	/bin/rm -f iparse.ml iparse.mli iparse.output lex.ml $(TARGET) *.cmi *.cmo *.mli

