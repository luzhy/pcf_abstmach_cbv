{
open Iparse
exception Lexer_Error of string
}


let space = [' ' '\n' '\t' '\r'] 
let digit = ['0'-'9']
let alpha = ['A'-'Z' 'a'-'z' '_']
let alnum = digit | alpha | '\''

rule token = parse
| digit+    {
              let str = Lexing.lexeme lexbuf in
                INT (int_of_string str) 
            }

| "Ldi"     { LDI }
| "Push"    { PUSH }
| "Add"     { ADD }
| "Sub"     { SUB }
| "Mult"    { MULT}
| "Div"     { DIV }
| "Test"    { TEST }
| "Extend"  { EXTEND }
| "Search"  { SEARCH }
| "Pushenv" { PUSHENV }
| "Popenv"  { POPENV }
| "Mkclos"  { MKCLOS }
| "Apply"   { APPLY }

| "["       { LBRAC }
| "]"       { RBRAC }
| "("       { LPAREN }
| ")"       { RPAREN }
| ","       { COMMA }
| "$"       { VAR ("$") }
| "$$"      { VAR ("$$") }

| alpha alnum*
            { VAR (Lexing.lexeme lexbuf) }

| eof       { EOF }


| space+    { token lexbuf }
           
| _
            {
              let message = Printf.sprintf
                              "Unknown token %s near characters %d-%d"
                              (Lexing.lexeme lexbuf)
                              (Lexing.lexeme_start lexbuf)
                              (Lexing.lexeme_end lexbuf)
              in
                raise (Lexer_Error message)
            }
