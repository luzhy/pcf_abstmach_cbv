%{
open Syntax
%}

%token <string>  VAR
%token <int>     INT

%token COMMA
%token LPAREN
%token RPAREN
%token LBRAC
%token RBRAC

%token LDI
%token PUSH
%token ADD
%token SUB
%token MULT
%token DIV
%token TEST
%token EXTEND
%token SEARCH
%token PUSHENV
%token POPENV
%token MKCLOS
%token APPLY

%token EOF 

%start main
%type <Syntax.instruction list> main

%%

main:
  | exps EOF
    { $1 }
;

exps:
  | exp
    { [$1] }
  | exp COMMA exps
    { $1 :: $3 }

exp:
  | LDI INT
    { Ins_Ldi($2) }
  
  | PUSH
    { Ins_Push }
  
  | ADD
    { Ins_Add }
  
  | SUB
    { Ins_Sub }
  
  | MULT
    { Ins_Mult }
  
  | DIV
    { Ins_Div }
  
  | TEST LPAREN LBRAC exps RBRAC COMMA LBRAC exps RBRAC RPAREN 
    { Ins_Test($4, $8) }
  
  | EXTEND VAR
    { Ins_Extend($2) }

  | SEARCH VAR
    { Ins_Search($2) }

  | PUSHENV
    { Ins_Pushenv }

  | POPENV
    { Ins_Popenv }
  
  | MKCLOS LPAREN VAR COMMA VAR COMMA LBRAC exps RBRAC RPAREN
    { Ins_Mkclos($3, $5, $8) }

  | APPLY
    { Ins_Apply }

  | error
    { 
      let message =
        Printf.sprintf 
          "Parse error near characters %d-%d"
          (Parsing.symbol_start ())
	      (Parsing.symbol_end ())
	  in
	    raise (Parser_Error message)
    }
;
