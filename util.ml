open Syntax;;

let pprint_ins instr = 
  let rec iter instr = 
    let matchi ins =
      match ins with
        | Ins_Ldi(x) -> 
            print_string ("Ldi" ^ " " ^ string_of_int(x))

        | Ins_Push -> 
            print_string "Push"

        | Ins_Add -> 
            print_string "Add"

        | Ins_Sub -> 
            print_string "Sub"

        | Ins_Mult -> 
            print_string "Mult"

        | Ins_Div -> 
            print_string "Div"

        | Ins_Test(i, j) ->
            print_string "Test";
            print_string "(";
            print_string "[";
            iter i ;
            print_string "]";
            print_string ", ";
            print_string "[";
            iter j ;
            print_string "]";
            print_string ")"

        | Ins_Extend(x) ->
            print_string ("Extend" ^ " " ^ x)

        | Ins_Search(x) -> 
            print_string ("Search" ^ " " ^ x)

        | Ins_Pushenv ->
            print_string "Pushenv"

        | Ins_Popenv ->
            print_string "Popenv"

        | Ins_Mkclos(f, x, t) ->
            print_string "Mkclos";
            print_string "(";
            print_string f;
            print_string ", ";
            print_string x;
            print_string ", ";
            print_string "[";
            iter t;
            print_string "]";
            print_string ")"

        | Ins_Apply ->
            print_string "Apply"
    in
      match instr with
        | [] -> ()
        | [ins_hd] -> matchi ins_hd
        | ins_hd :: ins_tl ->
            matchi ins_hd;
            print_string ", ";
            iter ins_tl
  in
    iter instr
;;

let pprint_val value = 
  match value with
    | Val_Const(n) -> 
        print_string "- : <Val_Const>   = ";
        print_int n;
        print_endline ""
    | Val_Closure(f,x,t,e) ->
        print_string "- : <Val_Closure> = ";
        print_string "Mkclos";
        print_string "(";
        print_string f;
        print_string ", ";
        print_string x;
        print_string ", ";
        print_string "[";
        pprint_ins t;
        print_string "]";
        print_string ")";
        print_endline ""
    | _ -> failwith "pprint error"
;;

