open Syntax;;

exception Eval_Error of string ;;

let choose_acc = function (a,s,e,c) -> a
let choose_stk = function (a,s,e,c) -> s
let choose_env = function (a,s,e,c) -> e
let choose_cod = function (a,s,e,c) -> c

let rec lookup x env =
  match env with
    | [] -> raise (Eval_Error ("Unbounded variable: " ^ x))
    | (y,v)::tl -> 
        if x = y then v 
        else lookup x tl 
;;

let rec eval fields = 
  let reg_a = choose_acc fields in
  let reg_s = choose_stk fields in
  let reg_e = choose_env fields in
  let reg_c = choose_cod fields in
    match reg_c with
      | [] -> fields
      | reg_c_hd :: reg_c_tl ->
          match reg_c_hd with
            | Ins_Ldi(n) -> 
                eval (Val_Const(n), reg_s, 
                      reg_e,        reg_c_tl)

            | Ins_Push ->
                eval (reg_a, (reg_a::reg_s), 
                      reg_e, reg_c_tl)

            | Ins_Add  -> 
                begin
                  match (reg_a,reg_s) with
                    | (Val_Const(n), (Val_Const(m) :: reg_s1)) ->
                        eval (Val_Const(n+m), reg_s1,
                              reg_e,          reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Add")
                end

             | Ins_Sub  -> 
                begin
                  match (reg_a,reg_s) with
                    | (Val_Const(n), (Val_Const(m) :: reg_s1)) ->
                        eval (Val_Const(n-m), reg_s1,
                              reg_e,          reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Sub")
                end

             | Ins_Mult  -> 
                begin
                  match (reg_a,reg_s) with
                    | (Val_Const(n), (Val_Const(m) :: reg_s1)) ->
                        eval (Val_Const(n*m), reg_s1,
                              reg_e,          reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Mult")
                end

             | Ins_Div  -> 
                begin
                  match (reg_a,reg_s) with
                    | (Val_Const(n), (Val_Const(m) :: reg_s1)) ->
                        eval (Val_Const(n/m), reg_s1,
                              reg_e,          reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Div")
                end

            | Ins_Mkclos(f, x, t) ->
                eval (Val_Closure(f, x, t, reg_e), reg_s,
                      reg_e,                       reg_c_tl)

            | Ins_Extend(x) ->
                begin
                  match reg_e with
                    | Val_Environ(reg_e1) ->
                        eval (reg_a, reg_s,
                              Val_Environ((x,reg_a)::reg_e1), reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Extend")
                end

            | Ins_Search(x) ->
                begin
                  match reg_e with
                    | Val_Environ(reg_e1) -> 
                        let v = lookup x reg_e1 in
                          eval (v,     reg_s,
                                reg_e, reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Search")
                end

            | Ins_Pushenv ->
                eval (reg_a, reg_e :: reg_s,
                      reg_e, reg_c_tl)

            | Ins_Popenv ->
                begin
                  match reg_s with
                    | Val_Environ(reg_e1) :: reg_s1 ->
                        eval (reg_a,  reg_s1,
                              Val_Environ(reg_e1), reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Popenv")
                end

            | Ins_Apply ->
                begin
                  match (reg_a, reg_s) with
                    | (Val_Closure(f, x, t, reg_e1), (w :: reg_s1)) ->
                        begin
                          match reg_e1 with
                            | Val_Environ(e1) ->
                                eval (reg_a,  reg_s1,
                                      Val_Environ((x, w) :: (f, reg_a) :: e1), 
                                      t @ reg_c_tl)
                            | _ -> raise (Eval_Error "wrong Apply1")
                        end
                    | _ -> raise (Eval_Error "wrong Apply2")
                end

            | Ins_Test(i, j) ->
                begin
                  match reg_a with
                    | Val_Const(0) -> 
                        eval (reg_a, reg_s,
                              reg_e, i @ reg_c_tl)
                    | Val_Const(n) -> 
                        eval (reg_a, reg_s,
                              reg_e, j @ reg_c_tl)
                    | _ -> raise (Eval_Error "wrong Test")
                end
;;
