open Syntax ;;
open Eval ;;
open Util;;

let parse_str str = 
  Iparse.main Lex.token 
    (Lexing.from_string str)
;;

let run_str str =
  choose_acc (eval (Val_Const(0), 
                    [], 
                    Val_Environ([]), 
                    (parse_str str)))
;;

let parse_file filename = 
  Iparse.main Lex.token
    (Lexing.from_channel (open_in filename))
;;

let run_file filename = 
  choose_acc (eval (Val_Const(0), 
                    [], 
                    Val_Environ([]), 
                    (parse_file filename))) 
;;

let run filename =
  pprint_val (run_file filename)
;;

let _ =
  pprint_val (run_file Sys.argv.(1))
;;

